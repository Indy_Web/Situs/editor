# Situs.run

## The Editor Module
The Editor module is inspired by every WYSIWYG/Markdown editor ever, espcially [Typora](https://typora.io/). The goal is to bring live web authorship directly to the browser.

This is a work-in-progress, not feature complete.

The Editor module is intended to be modular and therefore responsible only for modifying the DOM and not for saving the HTML document, which one may impliment in a variety of ways:

 * Writing to LocalStorage.
 * Storing to any kind of back-end technology.
 * Publishing directly to IPFS, which may allow for [trans-publishing](https://www.aus.xanadu.com/ted/TPUB/TPUBsum.html) across domains.

## Getting Started

To use the `editor.mjs` module:

```javascript:
<script type="module" src="https://gitlab.com/Indy_Web/Situs/editor/-/raw/main/editor.mjs">
   import Editor from '/editor.mjs';
   new Editor({ el: document.querySelector('article') });
</script>
```
Click anywhere within the editable element and begin writing.

Select parts of the text to modify inline styles or alter the block element.


## Architecture:
This is the third re-write of the Editor module.

The module was first written to be an "EditableElement" class, inheriting from HTMLElement, however in my experience hand-writen Javascript annotated with JSdoc requires manual type-checking via the `instanceof` operator, and this strategy of using class inheritance invites unnecessary complexity and technical resistance in the Javascript type system.

Secondly the module was re-written in the style espoused by Douglas Crockford: [class-free object-oriented programming](https://www.youtube.com/watch?v=XFTOG895C7c&t=2696s), "Javascript's gift to humanity," which was a great improvement in terms of simplicity, however the Editor was still designed around activating independent block elements, which majorly limits the capabilities of content editing by restricting the author's optionality to select and move content throughout the document and again creates unneccessary complexity.

Finally after a study of [Typester.io](https://typester.io/), created by MIT Media Lab, two important changes have been established:

 1. Rather than activating editable block elements individually and anchoring the editor toolbar on these elements, the Editor module has been re-written to utilize the Selection-Range as the source of truth for the area of the content being modified by the author, and the Editor toolbar simply anchors itself over the cursor selection. This design choice collapses the complexity of the module, which is great because the goal is _simplicity_.

 2. The Editor module has been re-written to utilize the _supposedly_ deprecated `document.execCommand`. At the time of writing, there is no viable alternative ([EditContext API](https://developer.mozilla.org/en-US/docs/Web/API/EditContext) is not stable), and use of `document.execCommand` is essential (in favor of direct DOM manipulation) in order to preserve the editor history state, which is provided by `contenteditable` elements, which is highly desirable for a legitimate rich text editor. Editors such as Google Docs, which are based on the `<canvas>` element are not suitable to the purpose of a WYSIWYG editor, which is intended to produce ready-to-serve HTML. This behavior is definitely guided by my [study of Typester](https://medium.com/typecode/the-typester-series-core-abc63affaef0), which describes "marshalling" HTML fragments out of the DOM for processing and back into the DOM utilizing `execCommand`. However this architecture is much simpler: Rather than using `<iframe>` for temporarily storing HTML fragments, the Situs.run Editor simply uses a DocumentFragment. Also the toolbar is totally encapsulated in the ShadowRoot of the editable element, which ensures that there is 0% DOM pollution —what you see _is_ what you get!

## To-do:

Here's a not-at-all-exhaustive list of some of the features that are intended to be written into the Editor module:

 * Nested Lists
 * Mobile-friendly support for Touch Events.
 * Typora-style Markdown shortcuts; eg., '#' => `<h1>`, etc.  
