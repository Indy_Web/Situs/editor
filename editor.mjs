// @ts-check

//? import DOMPurify from 'https://cdn.jsdelivr.net/npm/dompurify';

const debug = Debug(true)
const Φ = 1.618;
const φ = 1 / Φ;

export default class Editor {
   /** @type {Element} */  el;
   /** @type {HTMLElement} */  #toolbar;

   constructor({ el }) {
      if (!(el instanceof Element)) return;
      this.el = el;
      this.el.attachShadow({ mode: 'open' });
      this.el.shadowRoot?.append(document.createElement('slot'));
      this.el.shadowRoot?.append(toolbarTemplate().content.cloneNode(true));
      this.#toolbar = /** @type {HTMLElement} */
         (this.el.shadowRoot?.getElementById('toolbar'));
   }

   enable() {
      this.el.setAttribute('contenteditable', '')
      this.#addEventListeners();
   }

   disable() {
      this.el.removeAttribute('contenteditable');
      this.#removeEventListeners();
   }

   #listeners = [
      {
         eventType: 'beforeinput',
         handler: this.#handleBeforeInput
      },
      {
         eventType: 'selectstart',
         handler: this.#handleSelectStart.bind(this)
      },
      {
         eventType: 'keydown',
         handler: this.#handleKeyDown.bind(this)
      },
      {
         target: () => this.#toolbar,
         eventType: 'click',
         handler: this.#handleClickToolbar.bind(this)
      },
      {
         target: () => document.documentElement,
         eventType: 'click',
         handler: this.#handleClickOutside.bind(this)
      }
   ]

   #addEventListeners() {
      this.#listeners.forEach(listener => {
         const { target=()=>this.el, eventType, handler } = listener;
         target().addEventListener(eventType, handler)
      })
   }

   #removeEventListeners() {
      this.#listeners.forEach(listener => {
         const { target=()=>this.el, eventType, handler } = listener;
         target().removeEventListener(eventType, handler)
      })
   }

   #handleSelectStart() {
      document.addEventListener('mouseup', () => {
         const selection = document.getSelection();
         if (!selection || selection.isCollapsed) {
            this.#hideToolbar();
            return;
         }

         const rect = selection?.getRangeAt(0).getBoundingClientRect() || {}
         const left = (rect.width / 2) + rect.x
         const top = rect.top + document.documentElement.scrollTop

         this.#toolbar.style.top = `${top}px`;
         this.#toolbar.style.left = `${left}px`;
         this.#toolbar.style.opacity = '1';
      }, { once: true})
   }

   /** @param {KeyboardEvent} event */
   #handleKeyDown(event) {
      debug('keydown:', event.key)
      //? if (e.altKey)   this.classList.add('alt')
      //? if (e.ctrlKey)  this.classList.add('ctrl')
      //? if (e.shiftKey) this.classList.add('shift')
   
      //?if (e.key === 'Enter' && !e.shiftKey)
      //?    this.#insertParagraph(e)
      
      //?if (['Backspace', 'Delete'].includes(e.key))
      //?    this.#handleDeletion(e)
   

      if (event.key === 'Tab') {
         event.preventDefault();
         this.#indentBlock(!(event.shiftKey));
         // set cursor back to start position;

         //if (isCursorAtStart()) {}   //? or if surrounding Block element;

      }

      if (['ArrowUp', 'ArrowLeft'].includes(event.key)) {
         if (isCursorAtStart())
            navigateToPreviousSibling()
      }
   
      if (['ArrowDown', 'ArrowRight'].includes(event.key)) {
         if (isCursorAtEnd())
            navigateToNextSibling()
      }
   }

   /** @param {InputEvent} event */
   #handleBeforeInput(event) {
      debug('inputType:', event.inputType)
   }

   /** @param {MouseEvent} event */
   #handleClickOutside(event) {
      const $clicked = (event.target instanceof Element)
         ? event.target?.closest('*') : document.documentElement;
      if (!this.el.contains($clicked)) this.#hideToolbar();
      else requestAnimationFrame(() => {
         if (window.getSelection()?.isCollapsed)
            this.#hideToolbar();
      });
   }

   /** @param {MouseEvent} event */
   #handleClickToolbar(event) {
      event.stopPropagation();

      const $clicked = (/** @type {HTMLElement} */
         (event.target)).closest('li')
      if (!$clicked) return;
      else debug('clickedToolbar:', $clicked)

      //? Clicked on main menu or submenu item?
      if ($clicked.closest('menu')?.id === 'toolbar') {
         Array.from(this.#toolbar.children).forEach($child => {
               const $dropdown = (/** @type {HTMLElement} */ ($child.querySelector('menu.dropdown')))
               const $drawer = (/** @type {HTMLElement} */ ($child.querySelector('menu.drawer')))
               if (!$dropdown) return;
               if ($clicked === $child) {
                  $dropdown.style.visibility = ($dropdown.style.visibility === 'visible')
                     ? 'hidden' : 'visible'
               }
               else {
                  $dropdown.style.visibility = 'hidden'
                  if ($drawer) $drawer.style.visibility = 'hidden'
               }
         })
      }
      else {
         const { id } = $clicked
         debug('clicked: ', `#${id}`);

         const menu = id.split('-').shift() ?? ''
         if (menu === 'blocktext') {
            if (id === 'blocktext-quote')
               setBlockElement('blockquote');
            else if (id === 'blocktext-paragraph')
               setBlockElement('p');
            else if (id === 'blocktext-header') {
               let $drawer = /** @type {HTMLElement} */
                  (this.#toolbar.querySelector(`#${id} menu.drawer`))
               if (!$drawer) return;
               $drawer.style.visibility = ($drawer.style.visibility === 'visible')
                  ? 'hidden' : 'visible'
               }
         }
         else if (menu === 'style')
            formatStyle($clicked.id.split('-').pop())
         else if (menu === 'align')
            debug('clicked: menu#align'); //? this.setAlignment($clicked.id)
         else if (['h1', 'h2', 'h3', 'h4', 'h5', 'h6'].includes($clicked.id))
            setBlockElement($clicked.id)
      }
   }

   /** @param {boolean} increase */
   #indentBlock(increase=true) {
      modifyBlock((clone, target) => {
         if (!target) throw new Error("The `indentBlock` function requires `target` as second parameter.");
         const re = /^[\d]+/;
         const tabSize = Number(getComputedStyle(document.documentElement).getPropertyValue('--tab-size') ?? '2')
         const fontSize = String(getComputedStyle(document.documentElement)['font-size'] ?? '16px');
         const increment = tabSize * Number(re.exec(fontSize)?.shift() ?? '16');
   
         /** @param {number} px */
         const countTabs = (px) => (px - (px % increment)) / increment;
         const maxTabs = countTabs(Math.floor(this.el.clientWidth * φ));
         let numTabs = countTabs(Number(re.exec(getComputedStyle(target)['margin-left'])?.shift() || 0)) + (increase ? 1 : -1);
         if (numTabs > maxTabs) numTabs = maxTabs;
         clone.style.setProperty('--indent', String(numTabs))
         clone.style['margin-left'] = `calc(${increment}px * var(--indent))`;
         return clone;
      })
   }

   #hideToolbar() {
      this.#toolbar.style.opacity = '0';
      /** @type {HTMLElement[]} */
      (Array.from(this.#toolbar.querySelectorAll('menu:not(#toolbar)')))
         .forEach(menu => menu.style.visibility = 'hidden')
   }
}

/** @param {Node|any} [$] */
function getParentBlockElement($=getSelectionNode()) {
   if (!($ instanceof Node)) return null;
   while (!isBlockElement($)) {
      if (!$.parentElement) return null;
      $ = $.parentElement
   }
   return /** @type {Element | null} */(isBlockElement($) ? $ : null);
}

function getSelectionNode() {
   return getSelection()?.getRangeAt(0).commonAncestorContainer || null
}

/** @param {Element|any} $ */
function isBlockElement($) {
   if (!($ instanceof Element)) return false;
   return getComputedStyle($)?.display === 'block'
}

/** @param {Element|any} [$] */
function isCursorAtStart($) {
   $ = $ ?? getParentBlockElement();
   if (!($ instanceof Element) ||
       !($.firstChild)) return false;
   if (!$.innerHTML) return true;
   const selection = window.getSelection()
   return Boolean(
       selection &&
       selection.anchorNode === $.firstChild &&
       selection.anchorOffset === 0
   )
}

/** @param {Element|any} [$] */
function isCursorAtEnd($) {
   $ = $ ?? getParentBlockElement();
   if (!($ instanceof Element) ||
       !($.lastChild)) return false;
   if (!$.innerHTML) return true;
   const selection = window.getSelection()
   return Boolean(
       selection &&
       selection.anchorNode === $.lastChild &&
       selection.anchorOffset === $.lastChild.textContent?.trimEnd().length
   )
}

/** @param {Element|any} [$] */
function navigateToPreviousSibling($) {
   $ = $ ?? getParentBlockElement();
   if (!($ instanceof Element)) return;
   const $previous = $.previousElementSibling;
   if (!($previous instanceof Node
      && isBlockElement($previous))) return;

   debug('navigateToPreviousSibling()', $previous)
   window.requestAnimationFrame(() => {
      const selection = window.getSelection();
      const range = document.createRange();
      range.selectNodeContents($previous.lastChild || $previous);
      range.collapse(false);
      selection?.removeAllRanges();
      selection?.addRange(range);
   })
}

/** @param {Element|any} [$] */
function navigateToNextSibling($) {
   $ = $ ?? getParentBlockElement();
   if (!($ instanceof Element)) return;
   const $next = $.nextElementSibling;
   if (!($next instanceof Node
      && isBlockElement($next))) return;

   debug('navigateToNextSibling()', $next)
   window.requestAnimationFrame(() => {
      const selection = window.getSelection();
      const range = document.createRange();
      range.selectNodeContents($next.firstChild || $next);
      range.collapse(true);
      selection?.removeAllRanges();
      selection?.addRange(range);
   })
}

/** @param {string} localName */
function setBlockElement(localName) {
   if (!localName) return;
   const $block = getParentBlockElement();
   if (!$block) return;

   const range = new Range();
   const selection = getSelection();
   range.selectNode($block)
   selection?.removeAllRanges();
   selection?.addRange(range);
   document.execCommand('formatBlock', false, localName)
}

/** @param {string|any} style */
function formatStyle(style) {
   if (['bold', 'italic', 'underline', 'strikethrough'].includes(style))
      document.execCommand(style);
}

/** @param {(clone: HTMLElement, replaces?: HTMLElement) => HTMLElement} modify */
function modifyBlock(modify) {
   const target = getParentBlockElement();
   if (!(target instanceof HTMLElement)) return;

   const range = new Range();
   range.selectNode(target);
   const clone = range.cloneContents().firstElementChild;
   if (!(clone instanceof HTMLElement)) return;

   const modified = modify(clone, target)
   range.selectNodeContents(target)

   const selection = getSelection()
   selection?.removeAllRanges()
   selection?.addRange(range);
   document.execCommand('insertHTML', false, modified?.outerHTML);
   debug('modified:', modified?.outerHTML)
}




function Debug(debug=true) {
   return /** @type {(...data: any[]) => void} */ (debug
       ? (data) => {
            if (typeof data === 'object')
               console.dir(data)
            else
               console.log(data)
         }
       : () => {})
}


function toolbarTemplate() {
   const template = document.createElement('template');

const elementIcon = /* html */`
<svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 24 24"><path fill="currentColor" d="M9.71 6.29a1 1 0 0 0-1.42 0l-5 5a1 1 0 0 0 0 1.42l5 5a1 1 0 0 0 1.42 0a1 1 0 0 0 0-1.42L5.41 12l4.3-4.29a1 1 0 0 0 0-1.42Zm11 5l-5-5a1 1 0 0 0-1.42 1.42l4.3 4.29l-4.3 4.29a1 1 0 0 0 0 1.42a1 1 0 0 0 1.42 0l5-5a1 1 0 0 0 0-1.42Z"/></svg>`;

const styleIcon = /* html */`
<svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 2048 2048"><path fill="currentColor" d="M1055 896H609l-85 256H384L768 0h128l330 988l-106 105l-65-197zm-43-128L832 228L652 768h360zm581 131q42 0 78 14t63 41t42 61t16 79q0 39-15 76t-43 65l-717 717l-377 94l94-377l717-716q28-28 65-41t77-13zm50 246q21-21 21-51q0-32-20-50t-52-19q-14 0-27 4t-23 14l-692 692l-34 135l135-34l692-691z"/></svg>`;

const alignIcon = /* html */`
<svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 24 24"><path fill="currentColor" d="M3 15h10a1 1 0 0 0 0-2H3a1 1 0 0 0 0 2Zm0-4h10a1 1 0 0 0 0-2H3a1 1 0 0 0 0 2Zm0-4h18a1 1 0 0 0 0-2H3a1 1 0 0 0 0 2Zm18 10H3a1 1 0 0 0 0 2h18a1 1 0 0 0 0-2Zm-1.36-7.43a1 1 0 1 0-1.28 1.53l1.08.9l-1.08.9a1 1 0 0 0-.13 1.41a1 1 0 0 0 .77.36a1 1 0 0 0 .64-.24l2-1.66a1 1 0 0 0 0-1.54Z"/></svg>`;

const alignLeftIcon = /* html */`<svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 24 24"><path fill="currentColor" d="M3 7h18a1 1 0 0 0 0-2H3a1 1 0 0 0 0 2Zm0 4h14a1 1 0 0 0 0-2H3a1 1 0 0 0 0 2Zm18 2H3a1 1 0 0 0 0 2h18a1 1 0 0 0 0-2Zm-4 4H3a1 1 0 0 0 0 2h14a1 1 0 0 0 0-2Z"/></svg>`;

const alignCenterIcon = /* html */`<svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 24 24"><path fill="currentColor" d="M3 7h18a1 1 0 0 0 0-2H3a1 1 0 0 0 0 2Zm4 2a1 1 0 0 0 0 2h10a1 1 0 0 0 0-2Zm14 4H3a1 1 0 0 0 0 2h18a1 1 0 0 0 0-2Zm-4 4H7a1 1 0 0 0 0 2h10a1 1 0 0 0 0-2Z"/></svg>`;

const alignRightIcon = /* html */`<svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 24 24"><path fill="currentColor" d="M3 7h18a1 1 0 0 0 0-2H3a1 1 0 0 0 0 2Zm18 10H7a1 1 0 0 0 0 2h14a1 1 0 0 0 0-2Zm0-8H7a1 1 0 0 0 0 2h14a1 1 0 0 0 0-2Zm0 4H3a1 1 0 0 0 0 2h18a1 1 0 0 0 0-2Z"/></svg>`;

const alignJustifyIcon = /* html */`<svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 24 24"><path fill="currentColor" d="M3 7h18a1 1 0 0 0 0-2H3a1 1 0 0 0 0 2Zm18 10H3a1 1 0 0 0 0 2h18a1 1 0 0 0 0-2Zm0-4H3a1 1 0 0 0 0 2h18a1 1 0 0 0 0-2Zm0-4H3a1 1 0 0 0 0 2h18a1 1 0 0 0 0-2Z"/></svg>`;

const alignJustifyLeftIcon = /* html */`<svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 24 24"><path fill="currentColor" d="M3 5h18a1 1 0 0 0 0-2H3a1 1 0 0 0 0 2Zm12 14H3a1 1 0 0 0 0 2h12a1 1 0 0 0 0-2Zm6-8H3a1 1 0 0 0 0 2h18a1 1 0 0 0 0-2Zm0-4H3a1 1 0 0 0 0 2h18a1 1 0 0 0 0-2Zm0 8H3a1 1 0 0 0 0 2h18a1 1 0 0 0 0-2Z"/></svg>`;

const alignJustifyCenterIcon = /* html */`<svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 24 24"><path fill="currentColor" d="M21 15H3a1 1 0 0 0 0 2h18a1 1 0 0 0 0-2ZM3 5h18a1 1 0 0 0 0-2H3a1 1 0 0 0 0 2Zm14 14H7a1 1 0 0 0 0 2h10a1 1 0 0 0 0-2Zm4-12H3a1 1 0 0 0 0 2h18a1 1 0 0 0 0-2Zm0 4H3a1 1 0 0 0 0 2h18a1 1 0 0 0 0-2Z"/></svg>`;

const alignJustifyRightIcon = /* html */`<svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 24 24"><path fill="currentColor" d="M3 5h18a1 1 0 0 0 0-2H3a1 1 0 0 0 0 2Zm18 14H11a1 1 0 0 0 0 2h10a1 1 0 0 0 0-2Zm0-8H3a1 1 0 0 0 0 2h18a1 1 0 0 0 0-2Zm0 4H3a1 1 0 0 0 0 2h18a1 1 0 0 0 0-2Zm0-8H3a1 1 0 0 0 0 2h18a1 1 0 0 0 0-2Z"/></svg>`;

   template.innerHTML = /* html */`
<menu id="toolbar">
   <li id="blocktext-menu">
      ${elementIcon}
      <menu class="dropdown">
         <li title="header" id="blocktext-header">
            <span>#</span>
            <menu class="drawer">
               <li title="<h1>" id="h1"><span style="font-size: 1em;">H1</span></li>
               <li title="<h2>" id="h2"><span style="font-size: 0.9em;">H2</span></li>
               <li title="<h3>" id="h3"><span style="font-size: 0.8em;">H3</span></li>
               <li title="<h4>" id="h4"><span style="font-size: 0.7em;">H4</span></li>
               <li title="<h5>" id="h5"><span style="font-size: 0.6em;">H5</span></li>
               <li title="<h6>" id="h6"><span style="font-size: 0.5em;">H6</span></li>
            </menu>
         </li>
         <li title="paragraph" id="blocktext-paragraph"><span>¶</span></li>
         <li title="blockquote" id="blocktext-quote"><span>“”</span></li>
      </menu>
   </li>
   <li id="style-menu">
      ${styleIcon}
      <menu class="dropdown">
         <li title="bold" id="style-bold"><span style="font-weight: 700;">B</span></li>
         <li title="italic" id="style-italic"><span style="font-style: italic;">i</span></li>
         <li title="underline" id="style-underline"><span style="text-decoration: underline;">U</span></li>
         <li title="strikethrough" id="style-strikethrough"><span style="text-decoration: line-through;">S</span></li>
      </menu>
   </li>
   <li id="alignment-menu">
      ${alignIcon}
      <menu class="dropdown">
         <li id="align-left">
            ${alignLeftIcon}
         </li>
         <li id="align-center">
            ${alignCenterIcon}
         </li>
         <li id="align-right">
            ${alignRightIcon}
         </li>
         <li id="align-justify">
            ${alignJustifyIcon}
         </li>
         <li id="align-left-justify">
            ${alignJustifyLeftIcon}
         </li>
         <li id="align-center-justify">
            ${alignJustifyCenterIcon}
         </li>
         <li id="align-right-justify">
            ${alignJustifyRightIcon}
         </li>
      </menu>
   </li>
</menu>
<style>
menu {
   background-color: var(--_bg-color);
   border-radius: 0.5em;
   display: flex;
}

#toolbar {
   --_icon-size: var(--editor-toolbar-icon-size, 1.75em);
   --_caret-size: 6px;
   --_bg-color: var(--editor-toolbar-bg-color, var(--color-text-body, #2f1e07));
   --_icon-color: var(--editor-toolbar-icon-color, var(--color-bg-selection, #e3dcdc));
   --_icon-hover-color: var(--editor-toolbar-icon-hover-color, var(--color-bg-body, #ffffff));
   --_transition-duration: var(--editor-toolbar-transition-duration, 0.3s);

   position: absolute;
   margin: 0; padding: 0;

   transform: translate3d(-50%, calc(-100% - var(--_caret-size)), 0);

   flex-direction: row;
   justify-content: space-between;
   transition:
      top var(--_transition-duration) ease-in-out,
      left var(--_transition-duration) ease-in-out,
      opacity var(--_transition-duration) ease;

   opacity: 0;
   list-style-type: none;
   user-select: none;
   
   & li {
      display: grid;
      place-content: center;
      width: var(--_icon-size);
      padding: 0.2em;

      position: relative;

      & svg, span {
         width: var(--_icon-size);
         height: var(--_icon-size);
         color: var(--_icon-color);

         &:hover {
            color: var(--_icon-hover-color);
            cursor: pointer;
         }
      }

      & span {
         height: 1em;
         text-align: center;
         //line-height: var(--_icon-size);
         font-size: calc(0.75 * var(--_icon-size));
      }
      
      
      & menu.dropdown {
         position: absolute;
         margin: 0; padding: 0;
         top: 100%; left: 0;
         flex-direction: column;
         visibility: hidden;

         &> li {
            position: relative;
            & menu.drawer {
               position: absolute;
               top: 0; left: 100%;
               margin: 0; padding: 0;
               flex-direction: row;
               visibility: hidden;
               //height: 2em;

               &> li> span {
                  height: 100%;
               }
            }
         }

         &> li#blocktext-header {
            & menu.drawer {
               counter-reset: header;

               & li {
                  padding-inline-start: 0.2em;
                  padding-inline-end: 0;
               }
            }
         }

         & svg {
            width: calc(0.75 * var(--_icon-size));
            height: calc(0.75 * var(--_icon-size));
         }
      }
   }

   &::before {
      content: '';
      position: absolute;
      top: 100%;
      left: 50%;
      transform: translate3d(-50%, 0, 0);
      border-style: solid;
      border-width: var(--_caret-size) var(--_caret-size) 0;
      border-color: var(--_bg-color) transparent transparent;
   }
}
</style>
`
   return template;
}

// Todo:
// * Nested Lists
// * Disable clicking on links while editing article.
// * Click outside $editable => hide toolbar.
